import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'dart:async';                            //FUTURE
import 'dart:convert';                          //JSON
//void main() => runApp(MyApp());

const request = "https://api.hgbrasil.com/finance?format=json-cors&key=df0c8a9f";

//********************************** MAIN **************************************
void main () async{
  //http.Response response = await http.get(request);
  //print(response.body);
  //print(jsonDecode(response.body)["results"]["currencies"]["USD"]);

  print(await getData());

  runApp(MaterialApp(
    home: MyApp(),
    theme: ThemeData(
      hintColor: Colors.amber,          // DEFAULT SELECT
      primaryColor: Colors.white        // CLICK SELECT
    ),
  ));
}

//******************************** METHOD FUTURE *******************************
Future<Map> getData() async{
  http.Response response = await http.get(request);
  return json.decode(response.body);
}

//******************************** MY APP - DESIGNER ***************************
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  //******************* CONTROLLER
  final realController = TextEditingController();
  final dolarController = TextEditingController();
  final euroController = TextEditingController();

  //Variable
  double dolar;
  double euro;

  //************************************* METHOD CHANGE *********************
  void _realChanged(String text){
    //print(text);
    double real = double.parse(text);
    dolarController.text = (real/dolar).toStringAsFixed(2);
    euroController.text = (real/euro).toStringAsFixed(2);
  }
  void _dolarChanged(String text){
    //print(text);
    double dolar = double.parse(text);
    realController.text = (dolar*this.dolar).toStringAsFixed(2);
    euroController.text = (dolar*this.dolar/euro).toStringAsFixed(2);
  }
  void _euroChanged(String text){
    //print(text);
    double euro = double.parse(text);
    dolarController.text = (euro*this.euro/dolar).toStringAsFixed(2);
    realController.text = (euro*this.euro).toStringAsFixed(2);
  }

  //**************************** OVERRIDE WIDGET MAIN ********************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      //***************************** APP BAR *****************************
      appBar: AppBar(
        title: Text("\$ Conversor \$"),
        backgroundColor: Colors.amber,
        centerTitle: true,
      ),

      //***************************** BODY ********************************
      body: FutureBuilder<Map>(
        future: getData(),
        builder: (context, snapshot){
          //STATE CONNECTION
          switch(snapshot.connectionState){
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(
                child: Text(
                  "Carregando Dados...",
                  style: TextStyle(color: Colors.amber, fontSize: 25.0),
                  textAlign: TextAlign.center,
                ),
              );

            default:
              if(snapshot.hasError){
                return Center(
                  child: Text(
                    "Erro ao Carregar Dados :(",
                    style: TextStyle(color: Colors.amber, fontSize: 25.0),
                    textAlign: TextAlign.center,
                  ),
                );
              }else{
                dolar = snapshot.data["results"]["currencies"]["USD"]["buy"];
                euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];

                //return Container(color: Colors.green);
                return bodyDashboard();
              }
          }
        },

      ),

    );
  }

  //************************** CONTAINER - BODY FUTURE *************************
  Widget bodyFuture(){
    return FutureBuilder<Map>(
      future: getData(),
      builder: (context, snapshot){
        //STATE CONNECTION
        switch(snapshot.connectionState){
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Center(
              child: Text(
                "Carregando Dados...",
                style: TextStyle(color: Colors.amber, fontSize: 25.0),
                textAlign: TextAlign.center,
              ),
            );

          default:
            if(snapshot.hasError){
              return Center(
                child: Text(
                  "Erro ao Carregar Dados :(",
                  style: TextStyle(color: Colors.amber, fontSize: 25.0),
                  textAlign: TextAlign.center,
                ),
              );
            }else{
              dolar = snapshot.data["results"]["currencies"]["USD"]["buy"];
              euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];

              //return Container(color: Colors.green);
              return bodyDashboard();
            }
        }
      },
    );
  }

  //************************** CONTAINER - BODY DASHBOARD **********************
  Widget bodyDashboard() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //::::::::::::::::::::::::: IMAGE :::::::::::::::::::::
          Icon(Icons.monetization_on, size: 150.0, color: Colors.amber),

          //::::::::::::::::::: TEXT FIELD - REAIS ::::::::::::::::
          buildTextField("Reais", "R\$ ", realController, _realChanged),

          //:::::::::::::::::::::: SEPARATOR :::::::::::::::::::::
          Divider(),

          //::::::::::::::::::: TEXT FIELD - DOLAR ::::::::::::::::
          buildTextField("Dólares", "US\$ ", dolarController, _dolarChanged),

          //:::::::::::::::::::::: SEPARATOR :::::::::::::::::::::
          Divider(),

          //::::::::::::::::::: TEXT FIELD - EUROS ::::::::::::::::
          buildTextField("Euros", "€ ", euroController, _euroChanged)

        ],
      ),
    );
  }
//******************************************************************************
}

//************************** WIDGET TEXT FIELD - METHOD ************************
Widget buildTextField(String label, String prefix, TextEditingController c,
    Function f){
  return TextField(
    controller: c,
    decoration: InputDecoration(
      labelText: label,
      labelStyle: TextStyle(color: Colors.amber),
      border: OutlineInputBorder(),
      prefixText: prefix,
    ),
    style: TextStyle(
        color: Colors.amber, fontSize: 25.0
    ),

    onChanged: f,

    keyboardType: TextInputType.number,
  );
}
//******************************************************************************
